import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar/navbar.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {SpinnerComponent} from './spinner/spinner.component';
import {FooterComponent} from './footer/footer.component';
import {RouterModule} from '@angular/router';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [NavbarComponent, SidebarComponent, SpinnerComponent, FooterComponent],
  imports: [CommonModule, RouterModule, NgbCollapseModule, FormsModule],
  exports: [NavbarComponent, SidebarComponent, SpinnerComponent, FooterComponent]
})
export class SharedModule {
}
